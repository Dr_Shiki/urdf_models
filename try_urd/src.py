from urdfpy import URDF
from ikpy.chain import Chain
from ikpy.link import OriginLink, URDFLink
from ikpy import logs
from inv_kinematic import Inverse_kinematics
from scipy import interpolate
from leg_defenition import Leg
from pymitter import EventEmitter
from movement_planner import Movement_Planner
import time
import numpy
import threading

pi = 3.14
robot = URDF.load('t_hex_v2.URDF')
v, nodemap = robot.initialize_Animation(cfg={'leg1_1': 0,'leg1_2': 0,'leg1_3': pi/2})

#emitter = EventEmitter()

def on_Points_recieved(args):
    robot.Animate_Movements(v,nodemap,cfg=
    {
        'leg1_1': -args[0][0],'leg1_2': -args[0][1],'leg1_3': -args[0][2],
        'leg6_1': -args[1][0],'leg6_2': -args[1][1],'leg6_3': -args[1][2]
    })
    #print(args)


if __name__ == "__main__":
    time.sleep(2)
    planner = Movement_Planner(None)
    emitter = planner.Emitter
    emitter.on("newPoints",on_Points_recieved)
    planner.setAngle(pi/2)
    planner.setDuration(1)
    planner.setHeight(0.1)
    planner.setStep(0.07)
    planner.setFrenq(0.1)
    planner.setDuration(3)
    while(1):
        planner.walk()
