from scipy import interpolate
from leg_defenition import Leg
from scipy import interpolate
from pymitter import EventEmitter
from leg_manager import LegManager
import numpy
import time, threading
import math



class Movement_Planner:
    def __init__(self, Emitter):
        self.Emitter = EventEmitter()
        #self.Emitter = Emitter
        #self.First_Triple = [
        #    Leg("leg_1.json"),
        #    Leg("leg_3.json"),
        #    Leg("leg_4.json")
        #]
        #self.Second_Triple = [
        #    Leg("leg_2.json"),
        #    Leg("leg_4.json"),
        #    Leg("leg_6.json")
        #]

        self.TestLeg = [LegManager("leg_cfg/leg_1.json"),LegManager("leg_cfg/leg_2.json")]
    
    def setDuration(self, Duration):
        self.Duration = Duration
    
    def setHeight(self, Height):
        self.Height = Height

    def setStep(self,Step):
        self.Step = Step
    
    def setAngle(self,Angle):
        self.Angle = Angle
    
    def setFrenq(self, frenq):
        self.Frenq = frenq

    def _getBoundingPoints(self,point,step):
        x_finish = point[0] + (step/2)*math.cos(self.Angle)
        y_finish = point[1] + (step/2)*math.sin(self.Angle)

        x_start = point[0] - (step/2)*math.cos(self.Angle)
        y_start = point[1] - (step/2)*math.sin(self.Angle)

        x_arr = [x_start,point[0],x_finish]
        y_arr = [y_start,point[1],y_finish]

        return [x_arr,y_arr]

    def _getTrajectory(self, boundingPoints):
        S_points = [0, self.Step/2,self.Step]
        Z_points = [0,0.035,0]
        X_traj = interpolate.interp1d(S_points,boundingPoints[0])
        Y_traj = interpolate.interp1d(S_points,boundingPoints[1])
        Z_traj = interpolate.interp1d(S_points,Z_points)
        return [X_traj,Y_traj,Z_traj]

    def walk(self):
        flag = False
        count = 0
        while count < 2:
            for i in self.TestLeg:
                i.prepareTrajectory(self.Step,self.Angle,self.Height,flag)
            dTime = self.Duration/self.Frenq
            dStep = self.Step/dTime
            S=0
            while S<self.Step:
                #inv_points = self.TestLeg.getAngles(S)
                inv_points = []

                for i in self.TestLeg:
                    inv_points.append(i.getAngles(S))
                self.Emitter.emit("newPoints",inv_points)
                S+=dStep
                time.sleep(self.Frenq)
            flag = not flag