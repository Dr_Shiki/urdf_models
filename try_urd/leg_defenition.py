import json
import numpy
from inv_kinematic import Inverse_kinematics

class Leg:
    def __init__(self, jsonPath):
        with open(jsonPath) as json_file:
            self.config = json.load(json_file)
            self.pose = [0,0,0]
            self.angle = [0,0,0]
            self.height = 0

    def calculatePosition(self,x,y,z,height):
        if height-z >= 0:
            angles = [
                Inverse_kinematics.phi_1_1(x,y,z,height),
                Inverse_kinematics.phi_2_1(x,y,z,height),
                Inverse_kinematics.phi_3_1(x,y,z,height)]
        else:
            angles = [
                Inverse_kinematics.phi_1_1(x,y,z,height),
                Inverse_kinematics.phi_2_2(x,y,z,height),
                Inverse_kinematics.phi_3_2(x,y,z,height)]
        
        if self.config["Inverse"]:
            inverseAngles = []
            for i in angles:
                inverseAngles.append(-i)
            return inverseAngles
        self.pose = angles
        return angles
    
    def transformAngle(self,floatAngles):
        if self.config["Inverse"]:
            intAngles = [
                -int(floatAngles[0]*self.config["MultipleOffset"][0]+self.config["AddOffset"][0]),
                -int(floatAngles[1]*self.config["MultipleOffset"][1]+self.config["AddOffset"][0][1]),
                -int(floatAngles[2]*self.config["MultipleOffset"][2]+[2])]
            return intAngles
        else:
            intAngles = [
                int(floatAngles[0]*self.config.MultipleOffset[0]+self.config["AddOffset"][0][0]),
                int(floatAngles[1]*self.config.MultipleOffset[1]+self.config["AddOffset"][0][1]),
                int(floatAngles[2]*self.config.MultipleOffset[2]+self.config["AddOffset"][0][2])]
            return intAngles

    def Initial_Pose(self):
        return self.config["Initial_Pose"]