from scipy import interpolate
from leg_defenition import Leg
from scipy import interpolate
from pymitter import EventEmitter
import numpy
import time, threading
import math

class LegManager:
    def __init__(self, JSONpath):
        self.Leg = Leg(JSONpath)
    
    def _getBoundingPoints(self,step, angle):
        point = self.Leg.Initial_Pose()
        x_finish = point[0] + (step/2)*math.cos(angle)
        y_finish = point[1] + (step/2)*math.sin(angle)

        x_start = point[0] - (step/2)*math.cos(angle)
        y_start = point[1] - (step/2)*math.sin(angle)

        x_arr = [x_start,point[0],x_finish]
        y_arr = [y_start,point[1],y_finish]

        return [x_arr,y_arr]

    def _getTrajectory(self, boundingPoints, step, peek):
        Step = math.fabs(step)
        S_points = [0, Step/2,Step]
        Z_points = [0,peek,0]
        X_traj = interpolate.interp1d(S_points,boundingPoints[0])
        Y_traj = interpolate.interp1d(S_points,boundingPoints[1])
        Z_traj = interpolate.interp1d(S_points,Z_points)
        return [X_traj,Y_traj,Z_traj]

    def prepareTrajectory(self,step,angle,height, inverse = False):
        peek = 0.035
        if inverse:
            peek = 0
            step = -step
        self.height = height
        self.step = step
        points = self._getBoundingPoints(step,angle)
        self.trajectory = self._getTrajectory(points,step,peek)

    def getAngles(self,position):
        x = self.trajectory[0](position)
        y = self.trajectory[1](position)
        z = self.trajectory[2](position)
        return self.Leg.calculatePosition(x,y,z,self.height)